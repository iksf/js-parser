import React from 'react';
import ReactDOM from 'react-dom';
import ReactJson from "react-json-view";

import("../pkg/index.js").catch(console.error).then(f => {
    console.log("ferra loaded");
    const ferra = f.ferra;
    class App extends React.Component {
        constructor(props) {
            super(props);
            this.handleChange = this.handleChange.bind(this);
            this.state = {value: JSON.parse("{}"), time: 0}
        }

        render() {
            return (
                <div>
                    {this.state.time}ms
                    <form>
                        <textarea name={"text_area"} onChange={this.handleChange}/>
                    </form>
                    <ReactJson src={this.state.value} theme="monokai" />
                </div>

            )
        }

        handleChange(e) {
            const t0 = performance.now();
            const value = ferra(e.target.value);
            const t1 = performance.now();
            this.setState({value: JSON.parse(value), time: t1-t0})
            e.preventDefault();
        }
    }

    console.log(ferra);
    ReactDOM.render(
        <App/>,
        document.getElementById("root")
    )
});



