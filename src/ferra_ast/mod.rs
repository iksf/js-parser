use serde::Serialize;
use std::borrow::Cow;

#[derive(Debug, Clone, Serialize)]
pub enum LiteralType<'a> {
    String(Cow<'a, str>),
    Number(f64),
}

#[derive(Debug, Clone, Serialize)]
pub enum ExpressionStatementElement<'a> {
    Identifier(Identifier<'a>),
    Literal(Literal<'a>),
    CallExpression(CallExpression<'a>),
    MemberExpression(MemberExpression<'a>),
    BinaryExpression(BinaryExpression<'a>),
    AssignmentExpression(AssignmentExpression<'a>),
    ThisExpression(ThisExpression),
    UpdateExpression(UpdateExpression<'a>),
    ForOfStatement(ForOfStatement<'a>),
    ForInStatement(ForInStatement<'a>),
    ForStatement(ForStatement<'a>),
}


#[derive(Debug, Clone, Serialize)]
pub struct Identifier<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub name: Cow<'a, str>,
}

#[derive(Debug, Clone, Serialize)]
pub struct Literal<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub value: LiteralType<'a>,
}

#[derive(Debug, Clone, Serialize )]
pub enum ProgramBodyElement<'a> {
    ExpressionStatement(ExpressionStatement<'a>),
    VariableDeclaration(VariableDeclaration<'a>),
    BlockStatement(BlockStatement<'a>),
    ClassDeclaration(ClassDeclaration<'a>),
    FunctionDeclaration(FunctionDeclaration<'a>),
    ForOfStatement(ForOfStatement<'a>),
    ForInStatement(ForInStatement<'a>),
    ForStatement(ForStatement<'a>),
    SwitchStatement(SwitchStatement<'a>),
    WhileStatement(WhileStatement<'a>),
    DoWhileStatement(DoWhileStatement<'a>),
    ImportDeclaration(ImportDeclaration<'a>),
}



#[derive(Debug, Clone, Serialize)]
pub struct Program<'a>
{
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub body: Vec<ProgramBodyElement<'a>>,
    pub sourceType: &'static str,
}


#[derive(Debug, Clone, Serialize)]
pub struct ExpressionStatement<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub expression: ExpressionStatementElement<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum VariableDeclaratorInit<'a> {
    Literal(Literal<'a>),
    Identifier(Identifier<'a>),
    ClassExpression(ClassExpression<'a>),
    FunctionExpression(FunctionExpression<'a, BlockStatement<'a>>),
}

#[derive(Debug, Clone, Serialize)]
pub struct VariableDeclarator<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub id: Identifier<'a>,
    pub init: Option<VariableDeclaratorInit<'a>>,
}

#[derive(Debug, Clone, Serialize)]
pub struct VariableDeclaration<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub declarations: Vec<VariableDeclarator<'a>>,
    pub kind: Cow<'a, str>,
}

#[derive(Debug, Clone, Serialize)]
pub enum CallExpressionArgument<'a> {
    Literal(Literal<'a>),
    Identifier(Identifier<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum CallExpressionCallee<'a> {
    MemberExpression(MemberExpression<'a>),
    Identifier(Identifier<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct CallExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub callee: CallExpressionCallee<'a>,
    pub arguments: Vec<CallExpressionArgument<'a>>,
}

#[derive(Debug, Clone, Serialize)]
pub enum MemberExpressionObjectMember<'a> {
    Identifier(Identifier<'a>),
    MemberExpression(Box<MemberExpression<'a>>),
    ThisExpression(ThisExpression),
}

#[derive(Debug, Clone, Serialize)]
pub struct MemberExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub member: MemberExpressionObjectMember<'a>,
    pub value: Identifier<'a>,
    pub computed: bool,
}

#[derive(Debug, Clone, Serialize)]
pub enum BinaryExpressionLeftMember<'a> {
    BinaryExpression(Box<BinaryExpression<'a>>),
    Literal(Literal<'a>),
    Identifier(Identifier<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum BinaryExpressionRightMember<'a> {
    BinaryExpression(Box<BinaryExpression<'a>>),
    Literal(Literal<'a>),
    Identifier(Identifier<'a>),
    FunctionExpression(FunctionExpression<'a,BlockStatement<'a>>),
    ClassExpression(ClassExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct BinaryExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub left: BinaryExpressionLeftMember<'a>,
    pub operator: Cow<'a, str>,
    pub right: BinaryExpressionRightMember<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum BlockStatementMember<'a> {
    VariableDeclaration(VariableDeclaration<'a>),
    BlockStatement(Box<BlockStatement<'a>>),
    ExpressionStatement(ExpressionStatement<'a>),
    ForOfStatement(ForOfStatement<'a>),
    ForInStatement(ForInStatement<'a>),
    ForStatement(ForStatement<'a>),
    SwitchStatement(SwitchStatement<'a>),
    DoWhileStatement(DoWhileStatement<'a>),
    WhileStatement(WhileStatement<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct BlockStatement<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub body: Vec<BlockStatementMember<'a>>,
}

#[derive(Debug, Clone, Serialize)]
pub enum FunctionDeclarationParam<'a> {
    Identifier(Identifier<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct FunctionDeclaration<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub id: Identifier<'a>,
    pub expression: bool,
    pub generator: bool,
    pub r#async: bool,
    pub params: Vec<FunctionDeclarationParam<'a>>,
    pub body: BlockStatement<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct FunctionExpression<'a, BodyT> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub id: Option<Identifier<'a>>,
    pub expression: bool,
    pub generator: bool,
    pub r#async: bool,
    pub params: Vec<FunctionDeclarationParam<'a>>,
    pub body: BodyT,
}

#[derive(Debug, Clone, Serialize)]
pub enum SuperClassMember<'a> {
    Identifier(Identifier<'a>),
    MemberExpression(MemberExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct ClassDeclaration<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub id: Identifier<'a>,
    pub superClass: Option<SuperClassMember<'a>>,
    pub classBody: ClassBody<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ClassExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub id: Option<Identifier<'a>>,
    pub superClass: Option<SuperClassMember<'a>>,
    pub classBody: ClassBody<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum ClassBodyMember <'a>{
    MethodDefinition(MethodDefinition<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct ClassBody<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub body: Vec<ClassBodyMember<'a>>,
}

#[derive(Debug, Clone, Serialize)]
pub struct MethodDefinition<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub kind: &'static str,
    pub r#static: bool,
    pub computed: bool,
    pub key: Option<Identifier<'a>>,
    pub value: FunctionExpression<'a, BlockStatement<'a>>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ForInStatement<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub left: ForLeft<'a>,
    pub right: ForRight<'a>,
    pub body: BlockStatement<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum ForLeft<'a> {
    MemberExpression(MemberExpression<'a>),
    Identifier(Identifier<'a>),
    VariableDeclaration(VariableDeclaration<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum ForRight<'a> {
    Identifier(Identifier<'a>),
    MemberExpression(MemberExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct ForOfStatement<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub left: ForLeft<'a>,
    pub right: ForRight<'a>,
    pub body: BlockStatement<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum AssignmentExpressionRightMember<'a> {
    Literal(Literal<'a>),
    Identifier(Identifier<'a>),
    FunctionExpression(FunctionExpression<'a, BlockStatement<'a>>),
    ClassExpression(ClassExpression<'a>),
    MemberExpression(MemberExpression<'a>),
    BinaryExpression(BinaryExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum AssignmentExpressionLeftMember<'a> {
    Identifier(Identifier<'a>),
    MemberExpression(MemberExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct AssignmentExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub operator: Cow<'a, str>,
    pub left: AssignmentExpressionLeftMember<'a>,
    pub right: AssignmentExpressionRightMember<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ThisExpression {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
}

#[derive(Debug, Clone, Serialize)]
pub struct ForStatement<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub init: ForStatementInitMember<'a>,
    pub test: ForStatementTestMember<'a>,
    pub update: ForStatementUpdateMember<'a>,
    pub body: BlockStatement<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum ForStatementInitMember<'a> {
    Identifier(Identifier<'a>),
    BinaryExpression(BinaryExpression<'a>),
    VariableDeclarator(VariableDeclarator<'a>),
    VariableDeclaration(VariableDeclaration<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum UpdateExpressionArgument<'a> {
    Identifier(Identifier<'a>),
    MemberExpression(MemberExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct UpdateExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub operator: Cow<'a, str>,
    pub prefix: bool,
    pub argument: UpdateExpressionArgument<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum ForStatementTestMember<'a> {
    Identifier(Identifier<'a>),
    BinaryExpression(BinaryExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum ForStatementUpdateMember<'a> {
    Identifier(Identifier<'a>),
    UpdateExpression(UpdateExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub enum WhileStatementTest<'a> {
    BinaryExpression(BinaryExpression<'a>),
    Identifier(Identifier<'a>),
    MemberExpression(MemberExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct WhileStatement <'a>{
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub test: WhileStatementTest<'a>,
    pub body: BlockStatement<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct DoWhileStatement <'a>{
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub body: BlockStatement<'a>,
    pub test: WhileStatementTest<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct SwitchStatement<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub discriminant: Identifier<'a>,
    pub cases: Vec<SwitchCase<'a>>,
}

#[derive(Debug, Clone, Serialize)]
pub struct SwitchCase<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub consequent: Vec<ExpressionStatement<'a>>,
    pub test: Identifier<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ImportDeclaration<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub specifiers: ImportDefaultSpecifier<'a>,
    pub source: Literal<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ImportDefaultSpecifier<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub local: Identifier<'a>,
}

#[derive(Debug, Clone, Serialize)]
pub enum ArrowBody<'a> {
    BlockStatement(BlockStatement<'a>),
    Identifier(Identifier<'a>),
    Literal(Literal<'a>),
    ClassExpression(ClassExpression<'a>),
    FunctionExpression(FunctionExpression<'a ,BlockStatement<'a>>),
    ArrowFunctionExpression(Box<ArrowFunctionExpression<'a>>),
}

pub type ArrowFunctionExpression<'a> = FunctionExpression<'a, ArrowBody<'a>>;

#[derive(Debug, Clone, Serialize)]
pub struct ArrayExpression {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
}


#[derive(Debug, Clone, Serialize)]
pub enum ObjectExpressionProperty<'a> {
    Property(Property<'a>)
}

#[derive(Debug, Clone, Serialize)]
pub enum PropertyValue {

}

#[derive(Debug, Clone, Serialize)]
pub struct Property<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub method : bool,
    pub shorthand: bool,
    pub computed: bool,
    pub key: Identifier<'a>,
    pub value: PropertyValue,
    pub kind: Cow<'a, str>
}

#[derive(Debug, Clone, Serialize)]
pub struct ObjectExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub properties: Vec< ObjectExpressionProperty<'a>>
}


#[derive(Debug, Clone, Serialize)]
pub enum LogicalExpressionMember<'a> {
    Identifier(Identifier<'a>),
    LogicalExpression(Box<LogicalExpression<'a>>),
    BinaryExpression(BinaryExpression<'a>),
}

#[derive(Debug, Clone, Serialize)]
pub struct LogicalExpression<'a> {
    pub r#type: &'static str,
    pub start: usize,
    pub end: usize,
    pub left : LogicalExpressionMember<'a>,
    pub operator: Cow<'a, str>,
    pub right: LogicalExpressionMember<'a>,
}