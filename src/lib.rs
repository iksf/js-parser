use crate::parser::program;
use crate::span::Span;
use wasm_bindgen::prelude::*;
use web_sys::console;

// When the `wee_alloc` feature is enabled, this uses `wee_alloc` as the global
// allocator.
//
// If you don't want to use `wee_alloc`, you can safely delete this.



#[wasm_bindgen]
pub fn ferra(s:&str) -> String {
    let span = Span::new(s);
    let (_a, b) = program(span).unwrap();
    let c = serde_json::to_string_pretty(&b).unwrap();
    c
}



//pub mod ast;
pub mod acorn_ast;
pub mod error;
pub mod ferra_ast;
pub mod parser;
pub mod span;
pub mod util;
