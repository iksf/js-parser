use std::default::Default;
use std::iter::Enumerate;
use std::iter::Iterator;
use std::ops::RangeFrom;
use std::ops::RangeTo;
use std::ops::{Range, RangeInclusive, RangeToInclusive};
use std::str::Chars;

use nom::error::{ErrorKind, ParseError};
use nom::Err;
use nom::{
    AsBytes, Compare, CompareResult, FindSubstring, InputIter, InputLength, InputTake,
    InputTakeAtPosition, Needed, Offset, Slice,
};

/*A span of source
   data: slice of JS string
*/
#[derive(Clone, Debug, Copy)]
pub struct Span<'a> {
    pub data: &'a str,
    pub head_position: HeadPosition,
}

impl<'a> Span<'a> {
    pub(crate) fn start(&self) -> usize {
        self.head_position.offset
    }
    pub(crate) fn end(&self) -> usize {
        self.head_position.offset + self.input_len()
    }
}

/*
     offset: offset in chars from beginning of source,
     line: line of current read head,
     col: column of current read head
*/
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct HeadPosition {
    pub offset: usize,
    pub line: usize,
    pub col: usize,
}

impl<'a> PartialEq for Span<'a> {
    /*Compare positions for span comparison*/
    fn eq(&self, other: &Self) -> bool {
        self.head_position == other.head_position
    }
}

impl<'a> Default for Span<'a> {
    fn default() -> Self {
        Span {
            data: "",
            head_position: HeadPosition::default(),
        }
    }
}

impl Default for HeadPosition {
    fn default() -> Self {
        Self {
            offset: 0,
            line: 1,
            col: 1,
        }
    }
}

impl<'a> Span<'a> {
    /*Create new span with default positions based on input string ref*/
    pub fn new(data: &'a str) -> Span {
        Span {
            data,
            ..Self::default()
        }
    }

    /* Set the values of the span, offset, line, col, see struct definition for their importance
     */
    pub fn set_vals(self, offset: usize, line: usize, col: usize) -> Span<'a> {
        let head = HeadPosition { offset, line, col };
        Span {
            head_position: head,
            ..self /*move over the values from self to the new span*/
        }
    }
}
/*implement Nom InputLength trait to allow Nom to work out the length of the data string*/
impl<'a> InputLength for Span<'a> {
    fn input_len(&self) -> usize {
        self.data.len()
    }
}
/*Get a char iterator over the span's data
  Iterator is an enumeration, automatically tracking progress through the iteration in a usize
*/
impl<'a> InputIter for Span<'a> {
    type Item = char; /*Yield type*/
    type Iter = Enumerate<Self::IterElem>; /*Iterator type*/
    type IterElem = Chars<'a>;

    fn iter_indices(&self) -> Self::Iter {
        self.data.chars().enumerate()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.data.chars()
    }
    /*
        Find offset of beginning of a string slice that matches the predicate function
    */
    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.data.chars().position(|x| predicate(x))
    }

    /*Bounds check for data index access*/
    fn slice_index(&self, count: usize) -> Option<usize> {
        if count >= self.input_len() {
            Some(count)
        } else {
            None
        }
    }
}

/*
    This takes an immutable ref and should allow thread-safe forward searching for substrings
*/
impl<'a, 'b> FindSubstring<&'b str> for Span<'a>
where
    'b: 'a,
{
    fn find_substring(&self, substr: &'b str) -> Option<usize> {
        self.data.find_substring(substr)
    }
}

/*
  Nom equality operator, similar to PartialEq
*/
impl<'a, 'b> Compare<&'b str> for Span<'a> {
    fn compare(&self, t: &'b str) -> CompareResult {
        self.data.compare(t)
    }

    fn compare_no_case(&self, t: &'b str) -> CompareResult {
        self.data.compare_no_case(t)
    }
}

macro_rules! impl_slice_range {
    ($t:ty) => {
        /*Implement advancement of slice by slice for ranges */
        impl<'a> Slice<$t> for Span<'a> {
            fn slice(&self, range: $t) -> Self {
                let next = &self.data[range];
                let offset = self.data.offset(next);
                match offset {
                            0 => {
                                /*
                                    First slice or only slice, initial headPosition is correct
                                */
                                Span {
                                    data: next,
                                    head_position: self.head_position,
                                }
                            }
                            v /*
                                Number of chars advanced is > 0
                            */ => {
                                /*
                                    Update head position
                                */
                                let consumed: &str = &self.data[..v];
                                let new_head: HeadPosition =
                                    consumed
                                        .chars()
                                        .fold(self.head_position, |mut head: HeadPosition, c| {
                                            head.offset += 1;
                                            match c {
                                                '\n' => {
                                                    head.col = 1;
                                                    head.line += 1;
                                                }
                                                 _=> {
                                                    head.col += 1;
                                                }
                                            }
                                            head
                                        });
                                Span {
                                    data: next,
                                    head_position: new_head,
                                }
                            }
                        }
            }
        }
    };
}
impl_slice_range!(Range<usize>);
impl_slice_range!(RangeInclusive<usize>);
impl_slice_range!(RangeToInclusive<usize>);
impl_slice_range!(RangeTo<usize>);
impl_slice_range!(RangeFrom<usize>);

impl<'a> InputTake for Span<'a> {
    fn take(&self, count: usize) -> Self {
        self.slice(..count)
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        (self.slice(count..), self.slice(..count))
    }
}

impl<'a> AsBytes for Span<'a> {
    fn as_bytes(&self) -> &[u8] {
        self.data.as_bytes()
    }
}

impl<'a> InputTakeAtPosition for Span<'a> {
    type Item = char;

    fn split_at_position<P, E: ParseError<Self>>(
        &self,
        predicate: P,
    ) -> Result<(Self, Self), Err<E>>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.position(predicate) {
            Some(n) => Ok(self.take_split(n)),
            None => Err(nom::Err::Incomplete(Needed::Size(1))),
        }
    }

    fn split_at_position1<P, E: ParseError<Self>>(
        &self,
        predicate: P,
        e: ErrorKind,
    ) -> Result<(Self, Self), Err<E>>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.position(predicate) {
            Some(0) => Err(nom::Err::Error(E::from_error_kind(*self, e))),
            Some(n) => Ok(self.take_split(n)),
            None => Err(nom::Err::Incomplete(Needed::Size(1))),
        }
    }

    fn split_at_position_complete<P, E: ParseError<Self>>(
        &self,
        predicate: P,
    ) -> Result<(Self, Self), Err<E>>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.position(predicate) {
            Some(n) => Ok((self.slice(n..), self.slice(..n))),
            None => Ok(self.take_split(self.input_len())),
        }
    }

    fn split_at_position1_complete<P, E: ParseError<Self>>(
        &self,
        predicate: P,
        e: ErrorKind,
    ) -> Result<(Self, Self), Err<E>>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.position(predicate) {
            Some(0) => Err(Err::Error(E::from_error_kind(*self, e))),
            Some(n) => Ok((self.slice(n..), self.slice(..n))),
            None => {
                if self.input_len() == 0 {
                    Err(Err::Error(E::from_error_kind(*self, e)))
                } else {
                    Ok(self.take_split(self.input_len()))
                }
            }
        }
    }
}

impl<'a> Offset for Span<'a> {
    fn offset(&self, second: &Self) -> usize {
        second.head_position.offset - self.head_position.offset
    }
}

#[test]
fn test_span() {
    let a = Span::new("hello world");
    eprintln!("{:?}", a)
}
