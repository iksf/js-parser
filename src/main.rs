use std::convert::TryFrom;
use std::env;
use std::path::Path;

use parser::ferra;

fn main() -> Result<(), ()> {
    let args = env::args();
    let r = ferra("function x(){}");
    println!("{}", r);
    Ok(())
}
