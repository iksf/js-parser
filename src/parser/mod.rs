use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, alphanumeric1, multispace0, multispace1, space0, space1};
use nom::combinator::{map, map_parser, map_res, not, opt, peek, recognize};
use nom::IResult;
use nom::multi::{fold_many0, fold_many0c, fold_many1, many0, many1, many_m_n, many_till};
use nom::number::complete::recognize_float;
use nom::sequence::{delimited, preceded, terminated, tuple};
use wasm_bindgen::__rt::Start;

use crate::ferra_ast::{ArrowBody, ArrowFunctionExpression, AssignmentExpression, AssignmentExpressionLeftMember, AssignmentExpressionRightMember, BinaryExpression, BinaryExpressionLeftMember, BinaryExpressionRightMember, BlockStatement, BlockStatementMember, CallExpression, CallExpressionArgument, CallExpressionCallee, ClassBody, ClassBodyMember, ClassDeclaration, ClassExpression, DoWhileStatement, ExpressionStatement, ExpressionStatementElement, ForInStatement, ForLeft, ForOfStatement, ForRight, ForStatement, ForStatementInitMember, ForStatementTestMember, ForStatementUpdateMember, FunctionDeclaration, FunctionDeclarationParam, FunctionExpression, Identifier, ImportDeclaration, ImportDefaultSpecifier, Literal, LiteralType, MemberExpression, MemberExpressionObjectMember, MethodDefinition, Program, ProgramBodyElement, SuperClassMember, SwitchCase, SwitchStatement, ThisExpression, UpdateExpression, UpdateExpressionArgument, VariableDeclaration, VariableDeclarator, VariableDeclaratorInit, WhileStatement, WhileStatementTest};
use crate::span::Span;
use crate::ferra;

fn reserved_identifier(span: Span) -> IResult<Span, Span> {
    alt((
        tag("const"),
        tag("var"),
        tag("class"),
        tag("let"),
        tag("this"),
        tag("for"),
        tag("switch"),
        tag("async"),
        tag("function"),
        tag("while"),
        tag("case"),
        tag("import")
    ))(span)
}

fn identifier(span: Span) -> IResult<Span, Identifier> {
    map(
        tuple((
            span_position,
            not(reserved_identifier),
            alpha1,
            span_position,
        )),
        |(start, _, name, end)| Identifier {
            r#type: "Identifier",
            start,
            end,
            name: name.data.into(),
        },
    )(span)
}

#[test]
#[should_panic]
fn test_reserved() {
    let _x = identifier(Span::new("const")).unwrap();
}

fn map_number(span: Span) -> IResult<Span, Literal> {
    let value = span.data.parse::<f64>().map(LiteralType::Number).unwrap();
    Ok((
        span,
        Literal {
            r#type: "Literal",
            start: span.start(),
            end: span.end(),
            value,
        },
    ))
}

fn literal(span: Span) -> IResult<Span, Literal> {
    alt((
        map_parser(recognize_float, map_number),
        map_parser(recognize_string, map_string),
    ))(span)
}

fn recognize_string(span: Span) -> IResult<Span, Span> {
    let string_delimited_by = move |s| delimited(tag(s), alphanumeric1, tag(s));
    let string = alt((
        string_delimited_by("'"),
        string_delimited_by("\""),
        string_delimited_by("`"),
    ));
    string(span)
}

fn map_string(span: Span) -> IResult<Span, Literal> {
    let value = LiteralType::String(span.data.into());
    Ok((
        span,
        Literal {
            r#type: "Literal",
            start: span.start(),
            end: span.end(),
            value,
        },
    ))
}

fn expression_statement(span: Span) -> IResult<Span, ExpressionStatement> {
    alt((
        map(binary_expression, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::BinaryExpression(v),
        }),
        map(assignment_expression, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::AssignmentExpression(v),
        }),
        map(update_expression, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::UpdateExpression(v),
        }),
        map(call_expression, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::CallExpression(v),
        }),
        map(member_expression, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::MemberExpression(v),
        }),
        map(literal, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::Literal(v),
        }),
        map(this_expression, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::ThisExpression(v),
        }),
        map(identifier, |v| ExpressionStatement {
            r#type: "ExpressionStatement",
            start: v.start,
            end: v.end,
            expression: ExpressionStatementElement::Identifier(v),
        }),
    ))(span)
}

fn program_body_element(span: Span) -> IResult<Span, ProgramBodyElement> {
    use ProgramBodyElement::*;

    alt((
        map(import_declaration, ImportDeclaration),
        map(function_declaration, FunctionDeclaration),
        map(class_declaration, ClassDeclaration),
        map(for_of, ForOfStatement),
        map(for_in, ForInStatement),
        map(for_statement, ForStatement),
        map(block_statement, BlockStatement),
        map(variable_declaration, VariableDeclaration),
        map(expression_statement, ExpressionStatement),
        map(do_while_statement, DoWhileStatement),
        map(while_statement, WhileStatement),
        map(switch_statement, SwitchStatement),
    ))(span)
}

pub fn program(span: Span) -> IResult<Span, Program> {
    let expressions = many1(delimited(
        multispace0,
        program_body_element,
        tuple((space0, opt(tag(";")), multispace0)),
    ));
    map(expressions, |exp| Program {
        r#type: "Program",
        start: span.start(),
        end: span.end(),
        body: exp,
        sourceType: "module",
    })(span)
}

#[test]
fn test_program() {
    let span = Span::new(
        "var x = 5 \n
    var y = 3\n
    ",
    );
    let (_a, b) = program(span).unwrap();
    println!("{:#?}", b);
}
use std::borrow::Cow;

fn variable_declaration(span: Span) -> IResult<Span, VariableDeclaration> {
    match tuple((
        alt((tag("var"), tag("const"), tag("let"))),
        many1(preceded(
            delimited(space0, opt(tag(",")), space0),
            variable_declarator,
        )),
    ))(span)
    {
        Ok((remaining, (var, many_vec_declares))) => Ok((
            remaining,
            VariableDeclaration {
                r#type: "VariableDeclaration",
                start: span.start(),
                end: remaining.start(),
                declarations: many_vec_declares,
                kind:var.data.into()
            },
        )),
        Err(e) => Err(e),
    }
}

#[test]
fn test_var_declaration() {
    let span = Span::new("var x = 4, z = 3");
    let v = program(span);
    let k = v.unwrap().1;
    println!("{}", serde_json::to_string_pretty(&k).unwrap());
}

fn variable_declarator(span: Span) -> IResult<Span, VariableDeclarator> {
    match tuple((
        space0,
        identifier,
        space0,
        opt(tuple((
            tag("="),
            space0,
            alt((
                map(class_expression, VariableDeclaratorInit::ClassExpression),
                map(global_function_expression, VariableDeclaratorInit::FunctionExpression),
                map(literal, VariableDeclaratorInit::Literal),
                map(identifier, VariableDeclaratorInit::Identifier),
            )),
        ))),
    ))(span)
    {
        Ok((remaining, (_, ident, _, option))) => {
            let x = match option {
                Some((_, _, init)) => VariableDeclarator {
                    r#type: "VariableDeclarator",
                    start: span.start(),
                    end: remaining.start(),
                    id: ident,
                    init: Some(init.clone()),
                },
                None => VariableDeclarator {
                    r#type: "VariableDeclarator",
                    start: span.start(),
                    end: remaining.start(),
                    id: ident,
                    init: None,
                },
            };
            Ok((remaining, x))
        }
        Err(e) => Err(e),
    }
}

fn call_expression(span: Span) -> IResult<Span, CallExpression> {
    let callee = alt((
        map(member_expression, CallExpressionCallee::MemberExpression),
        map(identifier, CallExpressionCallee::Identifier),
    ));

    match tuple((
        callee,
        tag("("),
        many1(tuple((
            multispace0,
            alt((
                map(identifier, CallExpressionArgument::Identifier),
                map(literal, CallExpressionArgument::Literal),
            )),
            multispace0,
            opt(tag(",")),
            multispace0,
        ))),
        multispace0,
        tag(")"),
    ))(span)
    {
        Ok((remaining, (callee, _, maybe_many_lit_express, _, _))) => {
            let a = maybe_many_lit_express
                .iter()
                .map(|(_, lit, _, _, _)| lit.clone())
                .collect();
            Ok((
                remaining,
                CallExpression {
                    r#type: "CallExpression",
                    start: span.start(),
                    end: remaining.start(),
                    callee,
                    arguments: a,
                },
            ))
        }

        Err(e) => Err(e),
    }
}

pub fn this_expression(span: Span) -> IResult<Span, ThisExpression> {
    let a = tuple((span_position, tag("this"), span_position));
    map(a, |(start, _, end)| ThisExpression {
        r#type: "ThisExpression",
        start,
        end,
    })(span)
}

pub fn member_expression(span: Span) -> IResult<Span, MemberExpression> {
    let x = map(
        tuple((
            alt((
                map(identifier, MemberExpressionObjectMember::Identifier),
                map(
                    this_expression,
                    MemberExpressionObjectMember::ThisExpression,
                ),
            )),
            tag("."),
            identifier,
        )),
        |(a, _, b)| MemberExpression {
            r#type: "MemberExpression",
            start: span.start(),
            end: b.end,
            member: a,
            value: b,
            computed: false,
        },
    );

    let (a, b) = x(span)?;

    let e = fold_many0(tuple((tag("."), identifier)), b, |last, (_, d)| {
        let l = MemberExpression {
            r#type: "MemberExpression",
            start: span.start(),
            end: d.start,
            member: MemberExpressionObjectMember::MemberExpression(Box::new(last)),
            value: d,
            computed: false,
        };
        l
    });
    e(a)
}

#[test]
fn test_simple() {
    let span = Span::new("console.log");
    let v = program(span);
    let k = v.unwrap().1;
    println!("{}", serde_json::to_string_pretty(&k).unwrap());
}

#[test]
fn test_complex() {
    let span = Span::new("console.log.cat.dog");
    let v = program(span);
    let k = v.unwrap().1;
    println!("{}", serde_json::to_string_pretty(&k).unwrap());
}

#[test]
fn test_var_declare() {
    let span = Span::new("var x = 4 ");
    let v = program(span);
    let k = v.unwrap().1;
    println!("{}", serde_json::to_string_pretty(&k).unwrap());
}

#[test]
fn test_var_declare2() {
    let span = Span::new("var x = 5, y = 2");
    let v = program(span);
    let k = v.unwrap().1;
    println!("{}", serde_json::to_string_pretty(&k).unwrap());
}

fn operator_from<'a, 'b: 'a>(s: &'b str) -> impl Fn(Span<'a>) -> IResult<Span<'a>, String> {
    move |span| map(tag(s), |_| s.to_owned())(span)
}

fn operator(span: Span) -> IResult<Span, String> {
    let x = operator_from;
    alt((x("=="), x("+"), x("-"), x("/"), x("*"), x("<"), x(">")))(span)
}

#[test]
fn operator_test() {
    let span = Span::new("&&");
    let (_a, b) = operator(span).unwrap();
    println!("{}", b)
}

fn bracket_expression(span: Span) -> IResult<Span, BinaryExpression> {
    map(
        tuple((tag("("), space0, binary_expression, space0, tag(")"))),
        |(_, _, x, _, _)| x,
    )(span)
}

fn binary_expression_left_member(span: Span) -> IResult<Span, BinaryExpressionLeftMember> {
    alt((
        map(bracket_expression, |b| {
            BinaryExpressionLeftMember::BinaryExpression(Box::new(b))
        }),
        map(identifier, BinaryExpressionLeftMember::Identifier),
        map(literal, BinaryExpressionLeftMember::Literal),
    ))(span)
}

fn binary_expression_right_member(span: Span) -> IResult<Span, BinaryExpressionRightMember> {
    use BinaryExpressionRightMember::*;
    alt((
        map(bracket_expression, |b| {
            BinaryExpressionRightMember::BinaryExpression(Box::new(b))
        }),
        map(identifier, BinaryExpressionRightMember::Identifier),
        map(literal, BinaryExpressionRightMember::Literal),
        map(class_expression, ClassExpression),
        map(global_function_expression, FunctionExpression),
    ))(span)
}

fn binary_expression(span: Span) -> IResult<Span, BinaryExpression> {
    let (a, (b, _, c, _, d)) = tuple((
        binary_expression_left_member,
        space0,
        operator,
        space0,
        binary_expression_right_member,
    ))(span)?;
    let r = BinaryExpression {
        r#type: "BinaryExpression",
        start: span.start(),
        end: a.start(),
        left: b,
        operator: c.into(),
        right: d,
    };

    let (a, r) = fold_many0(
        tuple((
            span_position,
            space0,
            operator,
            space0,
            binary_expression_right_member,
            span_position,
        )),
        r,
        |last, (start, _, op, _, rhs, end)| BinaryExpression {
            r#type: "BinaryExpression",
            start,
            end,
            left: BinaryExpressionLeftMember::BinaryExpression(Box::new(last)),
            operator: op.into(),
            right: rhs,
        },
    )(a)?;
    Ok((a, r))
}

#[test]
fn binary_expression_test() {
    let span = Span::new("2 + 6 + 3 + 2 + 2 + 2");
    let x = program(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&x.1).unwrap())
}

#[test]
fn bracket_binary_expression_test() {
    let span = Span::new("2 + (3 + 3)");
    let x = program(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&x.1).unwrap())
}

fn block_statement_member(span: Span) -> IResult<Span, BlockStatementMember> {
    use BlockStatementMember::*;
    alt((
        map(variable_declaration, VariableDeclaration),
        map(expression_statement, ExpressionStatement),
        map(block_statement, |b| { BlockStatement(Box::new(b)) }),
        map(for_statement, ForStatement),
        map(for_in, ForInStatement),
        map(for_of, ForOfStatement),
        map(do_while_statement, DoWhileStatement),
        map(while_statement, WhileStatement),
        map(switch_statement, SwitchStatement)
    ))(span)
}

fn block_statement(span: Span) -> IResult<Span, BlockStatement> {
    let (remaining, (_, members, _)) = tuple((
        tag("{"),
        many0(preceded(
            multispace0,
            terminated(
                block_statement_member,
                opt(tuple((space0, tag(";"), multispace0))),
            ),
        )),
        preceded(multispace0, tag("}")),
    ))(span)?;
    let v = BlockStatement {
        r#type: "BlockStatement",
        start: span.start(),
        end: remaining.start(),
        body: members,
    };
    Ok((remaining, v))
}

#[test]
fn test_block_statement() {
    let span = Span::new("{}");
    let (_a, b) = program(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&b).unwrap())
}

#[test]
fn test_block_statement2() {
    let span = Span::new("{var x = 3\n2+2\nconsole.log('cat');}");
    let (_a, b) = program(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&b).unwrap())
}

fn function_params(span: Span) -> IResult<Span, Vec<FunctionDeclarationParam>> {
    many0(map(
        tuple((
            recognize(tuple((multispace0, opt(tag(",")), multispace0))),
            identifier,
        )),
        |(_, identifier)| FunctionDeclarationParam::Identifier(identifier),
    ))(span)
}


fn function_declaration(span: Span) -> IResult<Span, FunctionDeclaration> {
    /*
    todo clean this up
    */
    let (remaining, (asy, _, _, _, gen, _, id, _, _, _, f, _, _, _, body)) = tuple((
        opt(tag("async")),
        space0,
        tag("function"),
        space0,
        opt(tag("*")),
        space0,
        identifier,
        space0,
        tag("("),
        space0,
        function_params,
        space0,
        tag(")"),
        space0,
        block_statement,
    ))(span)?;

    let r#async = asy.is_some();
    let generator = gen.is_some();
    let r = FunctionDeclaration {
        r#type: "FunctionDeclaration",
        start: span.start(),
        end: remaining.start(),
        id,
        expression: false,
        generator,
        r#async,
        params: f,
        body,
    };

    Ok((remaining, r))
}

#[test]
fn test_global_function() {
    let span = Span::new("function x(){}");
    let b = program(span);
    println!("{:?}", b);
}

fn global_function_expression(span: Span) -> IResult<Span, FunctionExpression<BlockStatement>> {
    /*
    todo clean this up
    */
    let (remaining, (asy, _, _, _, gen, _, id, _, _, _, f, _, _, _, body)) = tuple((
        opt(tag("async")),
        multispace0,
        tag("function"),
        space0,
        opt(tag("*")),
        multispace0,
        opt(identifier),
        multispace0,
        tag("("),
        multispace0,
        function_params,
        multispace0,
        tag(")"),
        multispace0,
        block_statement,
    ))(span)?;

    let r#async = asy.is_some();
    let generator = gen.is_some();
    let r = FunctionExpression {
        r#type: "FunctionExpression",
        start: span.start(),
        end: remaining.start(),
        id,
        expression: false,
        generator,
        r#async,
        params: f,
        body,
    };

    Ok((span, r))
}

/// When using function expression in the class context FunctionExpression is used as an anonymous
/// value of a named MethodDefinition
fn class_function_expression(span: Span) -> IResult<Span, FunctionExpression<BlockStatement>> {
    let x = map(
        tuple((
            preceded(multispace0, opt(tag("async"))),
            preceded(multispace0, opt(identifier)),
            tag("("),
            multispace0,
            preceded(multispace0, function_params),
            tag(")"),
            multispace0,
            block_statement,
            span_position,
        )),
        |(a, i, _, _, f, _, _, b, end)| (a, i, f, b, end),
    );
    map(x, |(a, i, f, b, end)| FunctionExpression {
        r#type: "FunctionExpression",
        start: span.start(),
        end,
        id: i,
        expression: false,
        generator: false,
        r#async: match a {
            Some(_) => true,
            _ => false,
        },
        params: f,
        body: b,
    })(span)
}

#[test]
fn function_statement_test() {
    let span = Span::new("function* asd(x,y){x + y + z}");
    println!("{:#?}", function_declaration(span))
}

#[inline]
fn span_position(span: Span) -> IResult<Span, usize> {
    Ok((span, span.start()))
}

fn class_getter(span: Span) -> IResult<Span, MethodDefinition> {
    map(
        tuple((
            tag("get"),
            multispace1,
            identifier,
            multispace0,
            class_function_expression,
            span_position,
        )),
        |(_, _, id, _, value, end)| MethodDefinition {
            r#type: "MethodDefinition",
            start: span.start(),
            end: end,
            kind: "get".into(),
            r#static: false,
            computed: false,
            key: Some(id),
            value: value,
        },
    )(span)
}

#[test]
fn test_getter() {
    let span = Span::new("get x() {this.x}");
    let (_a, b) = class_getter(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&b).unwrap());
}

fn class_setter(span: Span) -> IResult<Span, MethodDefinition> {
    let (remaining, (_set, key, value)) = tuple((
        recognize(tuple((tag("set"), space1))),
        identifier,
        class_function_expression,
    ))(span)?;

    let r = MethodDefinition {
        r#type: "MethodDefinition",
        start: span.start(),
        end: remaining.end(),
        kind: "set",
        r#static: false,
        computed: false,
        key: Some(key),
        value,
    };

    Ok((remaining, r))
}

#[test]
fn test_setter() {
    let span = Span::new("set x(v) {this.p}");
    let (_a, b) = class_setter(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&b).unwrap());
}

fn class_constructor(span: Span) -> IResult<Span, MethodDefinition> {
    match tuple((
        tag("constructor"),
        preceded(multispace0, class_function_expression),
        span_position,
    ))(span)
    {
        Ok((remaining, (_, value, pos))) => {
            let r = MethodDefinition {
                r#type: "MethodDefinition",
                start: span.start(),
                end: pos,
                kind: "constructor",
                r#static: false,
                computed: false,
                key: None,
                value,
            };

            Ok((remaining, r))
        }
        Err(e) => Err(e),
    }
}

fn class_method(span: Span) -> IResult<Span, MethodDefinition> {
    map(
        tuple((
            span_position,
            identifier,
            multispace0,
            class_function_expression,
            span_position,
        )),
        |(start, id, _, value, end)| MethodDefinition {
            r#type: "MethodDefinition",
            start,
            end,
            kind: "method",
            r#static: false,
            computed: false,
            key: Some(id),
            value,
        },
    )(span)
}

fn class_body(span: Span) -> IResult<Span, ClassBody> {
    let a = alt((class_getter, class_setter, class_constructor, class_method));

    let b = tuple((
        recognize(tuple((multispace0, tag("{"), multispace0))),
        many0(map(
            preceded(multispace0, a),
            ClassBodyMember::MethodDefinition,
        )),
        recognize(tuple((multispace0, tag("}")))),
    ));

    let c = tuple((span_position, map(b, |(_, x, _)| x), span_position));

    let d = map(c, |(start, body, end)| ClassBody {
        r#type: "ClassBody",
        start,
        end,
        body,
    });
    d(span)
}
fn opt_superclass(span:Span) -> IResult<Span, Option<SuperClassMember>>   {
    use SuperClassMember::*;

    opt(map(
        tuple((multispace0, tag("extends"), multispace0, alt((
            map(member_expression,
                MemberExpression),
            map(identifier,
                Identifier)
        )))),
        |(_, _, _, c)| c,
    ))(span)
}

fn class_declaration(span: Span) -> IResult<Span, ClassDeclaration> {
    let a = tuple((
        span_position,
        tag("class"),
        multispace1,
        identifier,
        opt_superclass,
        multispace0,
        class_body,
        span_position,
    ));

    map(a, |(start, _, _, id, superClass, _, body, end)| {
        ClassDeclaration {
            r#type: "ClassDeclaration",
            start,
            end,
            id,
            superClass,
            classBody: body,
        }
    })(span)
}

fn class_expression(span: Span) -> IResult<Span, ClassExpression> {
   let a = tuple((
        span_position,
        tag("class"),
        multispace1,
        opt(identifier),
        opt_superclass,
        multispace0,
        class_body,
        span_position,
    ));

    map(a, |(start, _, _, id, superClass, _, body, end)| {
        ClassExpression {
            r#type: "ClassExpression",
            start,
            end,
            id,
            superClass,
            classBody: body,
        }
    })(span)
}

#[test]
fn class_body_test() {
    let s = "{
    constructor(y){}
    set x(y){this.x}
    get x(){this.x}
     }";

    let span = Span::new(s);
    let (_x, y) = class_body(span).unwrap();
    println!("{:#?}", y)
}

#[test]
fn class_test() {
    let s = "class X extends Y {\
    set x(y){this.x}\
    get x(){this.x}\
    constructor(x,y){this.z} \
    async f(x){this.x}\
     }";

    let span = Span::new(s);
    let (_x, y) = program(span).unwrap();
    println!("{}", serde_json::to_string_pretty(&y).unwrap())
}

fn assignment_expression(span: Span) -> IResult<Span, AssignmentExpression> {
    let lhs = alt((
        map(
            member_expression,
            AssignmentExpressionLeftMember::MemberExpression,
        ),
        map(identifier, AssignmentExpressionLeftMember::Identifier),
    ));

    let rhs = alt((
        map(
            class_expression,
            AssignmentExpressionRightMember::ClassExpression,
        ),
        map(
            global_function_expression,
            AssignmentExpressionRightMember::FunctionExpression,
        ),
        map(
            member_expression,
            AssignmentExpressionRightMember::MemberExpression,
        ),
        map(
            binary_expression,
            AssignmentExpressionRightMember::BinaryExpression,
        ),
        map(identifier, AssignmentExpressionRightMember::Identifier),
        map(literal, AssignmentExpressionRightMember::Literal),
    ));

    let operators = recognize(tuple((
        opt(alt((
            tag("+"),
            tag("-"),
            tag("/"),
            tag("%"),
            tag("*"),
            tag("="),
        ))),
        many_m_n(0, 2, tag("=")),
    )));

    map(
        tuple((
            span_position,
            preceded(space0, lhs),
            preceded(space0, operators),
            preceded(space0, rhs),
            span_position,
        )),
        |(start, lhs, operator, rhs, end)| AssignmentExpression {
            r#type: "AssignmentExpression",
            start,
            end,
            operator: operator.data.into(),
            left: lhs,
            right: rhs,
        },
    )(span)
}

#[test]
fn assignment_test() {
    let s = "x === 4";
    let span = Span::new(s);
    let (_x, y) = assignment_expression(span).unwrap();
    println!("{:#?}", y)
}

fn for_left(span: Span) -> IResult<Span, ForLeft> {
    use ForLeft::*;
    alt((
        map(member_expression, MemberExpression),
        map(identifier, Identifier),
        map(variable_declaration, VariableDeclaration),
    ))(span)
}

fn for_right(span: Span) -> IResult<Span, ForRight> {
    use ForRight::*;
    alt((
        map(member_expression, MemberExpression),
        map(identifier, Identifier), //todo object expression
    ))(span)
}

fn for_of(span: Span) -> IResult<Span, ForOfStatement> {
    map(
        tuple((
            span_position,
            tag("for"),
            multispace0,
            tag("("),
            for_left,
            multispace1,
            tag("of"),
            multispace1,
            for_right,
            multispace0,
            tag(")"),
            multispace0,
            block_statement,
            span_position,
        )),
        |(start, _, _, _, left, _, _, _, right, _, _, _, block, end)| ForOfStatement {
            r#type: "ForOfStatement",
            start,
            end,
            left,
            right,
            body: block,
        },
    )(span)
}

#[test]
fn test_for_of() {
    let span = Span::new("for (x of y) {}");
    let x = for_of(span);
    println!("{:?}", x)
}

fn for_in(span: Span) -> IResult<Span, ForInStatement> {
    map(
        tuple((
            span_position,
            tag("for"),
            multispace0,
            tag("("),
            for_left,
            multispace1,
            tag("in"),
            multispace1,
            for_right,
            multispace0,
            tag(")"),
            multispace0,
            block_statement,
            span_position,
        )),
        |(start, _, _, _, left, _, _, _, right, _, _, _, block, end)| ForInStatement {
            r#type: "ForInStatement",
            start,
            end,
            left,
            right,
            body: block,
        },
    )(span)
}

fn update_expression(span: Span) -> IResult<Span, UpdateExpression> {
    //todo impl prefix
    let a = alt((
        map(
            member_expression,
            UpdateExpressionArgument::MemberExpression,
        ),
        map(identifier, UpdateExpressionArgument::Identifier),
    ));

    let b = alt((tag("++"), tag("--")));

    let c = tuple((span_position, multispace0, a, space0, b, span_position));
    map(c, |(start, _, argument, _, operator, end)| {
        UpdateExpression {
            r#type: "UpdateExpression",
            start,
            end,
            operator: operator.data.into(),
            prefix: false,
            argument,
        }
    })(span)
}

fn for_statement(span: Span) -> IResult<Span, ForStatement> {
    let a = recognize(tuple((tag("for"), space0, tag("("), space0)));
    let b = recognize(tuple((space0, tag(";"), space0)));

    let c = recognize(tuple((space0, tag(";"), space0)));
    let d = recognize(tuple((space0, tag(")"), space0)));
    let e = tuple((
        span_position,
        preceded(
            a,
            alt((
                map(binary_expression, ForStatementInitMember::BinaryExpression),
                map(identifier, ForStatementInitMember::Identifier),
                map(
                    variable_declaration,
                    ForStatementInitMember::VariableDeclaration,
                ),
            )),
        ),
        preceded(
            b,
            alt((
                map(binary_expression, ForStatementTestMember::BinaryExpression),
                map(identifier, ForStatementTestMember::Identifier),
            )),
        ),
        preceded(
            c,
            alt((
                map(
                    update_expression,
                    ForStatementUpdateMember::UpdateExpression,
                ),
                map(identifier, ForStatementUpdateMember::Identifier),
            )),
        ),
        preceded(d, block_statement),
        span_position,
    ));

    map(e, |(start, init, test, update, body, end)| ForStatement {
        r#type: "ForStatement",
        start,
        end,
        init,
        test,
        update,
        body,
    })(span)
}

#[test]
fn test_for_statement() {
    let span = Span::new("for (x;x<5;x++){}   ");
    let (_a, b) = for_statement(span).unwrap();
    let x = serde_json::to_string_pretty(&b).unwrap();
    println!("{}", x)
}

fn while_statement(span: Span) -> IResult<Span, WhileStatement> {
    let x = tuple((
        span_position,
        recognize(tuple((tag("while"), space0, tag("("), space0))),
        alt((
            map(binary_expression, WhileStatementTest::BinaryExpression),
            map(member_expression, WhileStatementTest::MemberExpression),
            map(identifier, WhileStatementTest::Identifier),
        )),
        recognize(tuple((space0, tag(")"), space0))),
        block_statement,
        span_position,
    ));

    let v = map(x, |(start, _, test, _, body, end)| WhileStatement {
        r#type: "WhileStatement",
        start,
        end,
        test,
        body,
    });

    v(span)
}

#[inline]
fn do_while_statement(span: Span) -> IResult<Span, DoWhileStatement> {
    let a = recognize(tuple((multispace0, tag("do"), multispace0)));

    let b = recognize(tuple((tag("while"), multispace0, tag("("))));
    let x = tuple((
        span_position,
        a,
        block_statement,
        b,
        alt((
            map(binary_expression, WhileStatementTest::BinaryExpression),
            map(member_expression, WhileStatementTest::MemberExpression),
            map(identifier, WhileStatementTest::Identifier),
        )),
        tag(")"),
        span_position,
    ));

    let v = map(x, |(start, _, body, _, test, _, end)| DoWhileStatement {
        r#type: "DoWhileStatement",
        start,
        end,
        body,
        test,
    });
    v(span)
}

#[inline]
fn switch_statement(span: Span) -> IResult<Span, SwitchStatement> {
    let x = delimited(
        tuple((tag("switch"), multispace0, tag("("), multispace0)),
        identifier,
        tuple((multispace0, tag(")"), multispace0)),
    );

    let cases = delimited(
        tag("{"),
        many0(preceded(multispace0, switch_case)),
        preceded(multispace0, tag("}")),
    );

    let a = tuple((span_position, x, cases, span_position));

    let b = map(a, |(start, n, m, end)| SwitchStatement {
        r#type: "SwitchStatement",
        start,
        end,
        discriminant: n,
        cases: m,
    });
    b(span)
}

fn switch_case(span: Span) -> IResult<Span, SwitchCase> {
    let a = delimited(tuple((tag("case"), space0)), identifier, tag(":"));

    let b = many0(preceded(multispace0, expression_statement));

    let c = tuple((span_position, a, b, span_position));

    let d = map(c, |(start, test, consequent, end)| SwitchCase {
        r#type: "SwitchCase",
        start,
        end,
        consequent,
        test,
    });

    d(span)
}


fn import_declaration(span: Span) -> IResult<Span, ImportDeclaration> {
    let x = tuple((
        span_position,
        tag("import"),
        space0,
        tuple((span_position, identifier, span_position)),
        space0,
        tag("from"),
        space0,
        literal,
        space0,
        opt(tag(";")),
        span_position
    ));

    map(x, |(start, _, _, (a, b, c), _, _, _, d, _, _, end)| {
        ImportDeclaration {
            r#type: "ImportDeclaration",
            start,
            end,
            specifiers: ImportDefaultSpecifier {
                r#type: "ImportDefaultSpecifier",
                start: a,
                end: c,
                local: b,
            },
            source: d,
        }
    })(span)
}

#[test]
fn test_import() {
    let span = Span::new("import a from 'react'");
    let (_a, b) = import_declaration(span).unwrap();

    let c = serde_json::to_string_pretty(&b).unwrap();

    println!("{}", c);
}



#[cfg(test)]
mod tests {
    use criterion::Bencher;
    use crate::ferra;

    fn bench_parser(b: &mut Bencher) {
        let test_prog = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/bench/bench1.js"));
        b.iter(|| ferra(test_prog));
    }
}
