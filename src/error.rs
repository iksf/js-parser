use std::fmt::Display;

use nom::error::ErrorKind;
use nom::lib::std::fmt::Formatter;

#[derive(Debug, PartialEq)]
pub enum Error {
    SerdeErr(String),
    ParseFloatErr(std::num::ParseFloatError),
    NomErrors(Vec<(String, ErrorKind)>),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::SerdeErr(s) => f.write_str(s),
            Error::ParseFloatErr(s) => f.write_str(&format!("ParseFloatErr : {:#?}", s)),
            _ => panic!(),
        }
    }
}

impl std::error::Error for Error {}
