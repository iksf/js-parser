use criterion::{black_box, Criterion, criterion_group, criterion_main};
use parser::ferra;




fn fibonacci(n: u64) -> u64 {
    match n {
        0 => 1,
        1 => 1,
        n => fibonacci(n - 1) + fibonacci(n - 2),
    }
}


pub fn ferra_bench(
    c: &mut Criterion
)  {
    let s = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/bench/bench1.js"));

    c.bench_function("bench1", |b| b.iter(|| ferra(black_box(s))));
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("fib 20", |b| b.iter(|| fibonacci(black_box(20))));
}

criterion_group!(benches, ferra_bench);
criterion_main!(benches);